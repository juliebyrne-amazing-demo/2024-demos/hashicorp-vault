# Hashicorp Vault

![Project Stage][project-stage-shield]
![Maintenance][maintenance-shield]
![Pipeline Status][pipeline-shield]


[![License][license-shield]](LICENSE.md)
[![Maintaner][maintainer-shield]](https://gitlab.com/jsandlin)

## Table of Contents
[[_TOC_]]

## Overview

1. When referencing local software installation ([Homebrew](https://brew.sh/)), this instruction set is written for macOS.
2. LetsEncrypt SSL registration / generation / management
   - This generates `pem` files.
     - `privkey.pem` - Private key for the certificate.
     - `fullchain.pen` - All certificates, including server certificate (aka leaf certificate or end-entity certificate).
     - `cert.pem` - The server certificate alone
     - `chain.pem` - The additional intermediate certificate or certificates
3. Hashicorp Vault Terraform

## PreReq
1. [Service Account](https://gitlab.com/sandlin/terraform/sa_management)
2. [Create domain in GCP](https://gitlab.com/sandlin/terraform/gcp_dns)
3. [Homebrew](https://brew.sh/) is installed on your system.
4. Download the root certificate from [LetsEncrypt Certificates](https://letsencrypt.org/certificates/)
   - I downloaded the `ISRG Root X1 / Self-signed / pem` file.
   - Save it as `ca.crt`
5. The first time I ran the TF, it created a Bucket `vault-certs`, a Key Ring `vault`, & a Key `vault-init`. 
   When you delete an env via TF, GCP does not delete buckets with history enabled or key rings. Therefore,
   when I deleted the initial env, I went into GCP and re-enabled these services so I could use them moving forward. 
   1. TODO: Automate creation of a bucket + a key ring + a key, which will be used to store the cert files.
      OR how to better utilize the vault module.
   2. [crypto.tf](https://github.com/terraform-google-modules/terraform-google-vault/blob/master/modules/cluster/crypto.tf)
  
   
## Usage

### Install Vault CLI Locally
> Note: This is written for MacOS. 
1. Configure Homebrew to reference `hashicorp/tap`
    ```shell
    % brew tap hashicorp/tap
    Updating Homebrew...
    ...
    Tapped 1 cask and 11 formulae (42 files, 404.1KB).
    ```
2. Install Hashicorp Vault CLI
    ```
    % brew install hashicorp/tap/vault
    ==> Downloading https://releases.hashicorp.com/vault/1.8.4/vault_1.8.4_darwin_amd64.zip
    ...
    ```

-----------------
### LetsEncrypt
1. I believe I'm missing a step here in registering with letsencrypt. Need to add it.
2. Generate the certs
   ```
    DOMAIN=vault.in-betweener.com
    docker run -it --rm --name certbot \
    -v "/Users/jsandlin/.gcp/dns-admin.json:/opt/dns-admin.json" \
    -v "/Users/jsandlin/workspaces/sandlin/terraform/hashicorp_vault/letsencrypt/outputs/certs:/etc/letsencrypt" \
    -v "/Users/jsandlin/workspaces/sandlin/terraform/hashicorp_vault/letsencrypt/outputs/logs:/var/log" \
    certbot/dns-google certonly \
    --dns-google \
    --dns-google-credentials /opt/dns-admin.json \
    --dns-google-propagation-seconds 90 \
    --server https://acme-v02.api.letsencrypt.org/directory \
    --agree-tos -m "james@sandlininc.com" --non-interactive \
    -d ${DOMAIN}
   ```
    1. EXAMPLE: 
       ```
        jsandlin% export DOMAIN=vault.in-betweener.com
        jsandlin% docker run -it --rm --name certbot \
        >     -v "/Users/jsandlin/.gcp/dns-admin.json:/opt/dns-admin.json" \
        >     -v "/Users/jsandlin/workspaces/sandlin/terraform/hashicorp_vault/letsencrypt/outputs/certs:/etc/letsencrypt" \
        >     -v "/Users/jsandlin/workspaces/sandlin/terraform/hashicorp_vault/letsencrypt/outputs/logs:/var/log" \
        >     certbot/dns-google certonly \
        >     --dns-google \
        >     --dns-google-credentials /opt/dns-admin.json \
        >     --dns-google-propagation-seconds 90 \
        >     --server https://acme-v02.api.letsencrypt.org/directory \
        >     --agree-tos -m "james@sandlininc.com" --non-interactive \
        >     -d ${DOMAIN}
        Saving debug log to /var/log/letsencrypt/letsencrypt.log
        Requesting a certificate for vault.in-betweener.com
        Waiting 90 seconds for DNS changes to propagate
        
        Successfully received certificate.
        Certificate is saved at: /etc/letsencrypt/live/vault.in-betweener.com/fullchain.pem
        Key is saved at:         /etc/letsencrypt/live/vault.in-betweener.com/privkey.pem
        This certificate expires on 2022-01-26.
        These files will be updated when the certificate renews.
        
        NEXT STEPS:
        - The certificate will need to be renewed before it expires. Certbot can automatically renew the certificate in the background, but you may need to take steps to enable that functionality. See https://certbot.org/renewal-setup for instructions.
        
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        If you like Certbot, please consider supporting our work by:
         * Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
         * Donating to EFF:                    https://eff.org/donate-le
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       ```
    - To view the generated files:
      ```
      jsandlin% ls  ./outputs/certs/live/vault-in-betweener.com
      README          cert.pem        chain.pem       fullchain.pem   privkey.pem
      ```
3. Copy generated certs to localdir for easier use.
   - Also renaming them due to the Vault's TF standard.
     ```shell
     rm ./vault_key.pem ./vault.crt ./vault.key.enc
     cp ./letsencrypt/outputs/certs/live/vault.in-betweener.com/privkey.pem ./vault_key.pem
     cp ./letsencrypt/outputs/certs/live/vault.in-betweener.com/fullchain.pem ./vault.crt
     ```
4. Encrypt the root certificate
     ```
     KMS_KEY=vault-init;
     KMS_KEYRING=vault;
     KMS_LOCATION=us-west1;
     GCP_PROJECT=jsandlin-c9fe7132
     # ENCRYPT OUR VAULT KEY
     gcloud kms encrypt \
       --project=${GCP_PROJECT} \
       --key=${KMS_KEY} \
       --keyring=${KMS_KEYRING} \
       --location=${KMS_LOCATION} \
       --plaintext-file=./vault_key.pem \
       --ciphertext-file=- | base64 > "vault.key.enc";
     ```
5. Upload cert files to the bucket for app use.
   ```
   BUCKET=vault-certs
    
   for file in vault.key.enc ca.crt vault.crt; do
       gsutil cp $file gs://$BUCKET/$file
   done
   ```

Now it's time to setup Vault.

-----------------
### Hashicorp Vault
1. Modify [terraform.tfvars](./terraform.tfvars) && [install.sh](./install.sh) accordingly.
   - TODO: Extend this regarding which vars to change & why
2. Execute the Terraform.
   - I run locally so use [install.sh](install.sh).

Example output:

```shell
% ./install.sh create
...

Apply complete! Resources: 0 added, 1 changed, 0 destroyed.

Outputs:

service_account_email = "hashicorp-vault@jsandlin-c9fe7132.iam.gserviceaccount.com"
vault_addr = "https://35.199.189.110:8200"
vault_lb_addr = "35.199.189.110"
vault_lb_port = "8200"
vault_nat_ips = [
  "34.82.25.32",
  "34.127.97.36",
]
vault_network = "https://www.googleapis.com/compute/v1/projects/jsandlin-c9fe7132/global/networks/vault-network"
vault_storage_bucket = "jsandlin-c9fe7132-vault-data"
vault_subnet = "https://www.googleapis.com/compute/v1/projects/jsandlin-c9fe7132/regions/us-west1/subnetworks/vault-subnet"

```

-----------------
### Base configuration of Vault.
1. Create an environment variable `VAULT_ADDR` and assign your Vault URL.
   ```
   % grep "VAULT_ADDR" ~/.zshrc
   export VAULT_ADDR="https://vault.in-betweener.com:8200"
   ```
2. Verify your connection
   ```shell
    % vault status
    Key                      Value
    ---                      -----
    Recovery Seal Type       gcpckms
    Initialized              false
    Sealed                   true
    Total Recovery Shares    0
    Threshold                0
    Unseal Progress          0/0
    Unseal Nonce             n/a
    Version                  1.6.0
    Storage Type             gcs
    HA Enabled               true
    ```
3. Initialize Vault.
   - DO NOT SHARE THESE KEYS / TOKEN AND DO NOT LOSE THEM. I stored mine in 1Password.
     ```shell
        % vault operator init \
        -recovery-shares 5 \
        -recovery-threshold 3
        Recovery Key 1: foobar1
        Recovery Key 2: foobar2
        Recovery Key 3: foobar3
        Recovery Key 4: foobar4
        Recovery Key 5: foobar5
        
        Initial Root Token: foobar
        
        Success! Vault is initialized
        
        Recovery key initialized with 5 key shares and a key threshold of 3. Please
        securely distribute the key shares printed above.
     ```
4. Log into Vault via the Initial Root Token:
   ```
   % vault login -method=token
   Token (will be hidden):
   Success! You are now authenticated. The token information displayed below
   is already stored in the token helper. You do NOT need to run "vault login"
   again. Future Vault requests will automatically use this token.
   
    Key                  Value
    ---                  -----
    token                s.EEOfBbs8j3okAfn9EQYdrO5a
    token_accessor       T0I2tFAlCWEXC5DIoIth8fdF
    token_duration       ∞
    token_renewable      false
    token_policies       ["root"]
    identity_policies    []
    policies             ["root"]
    ```
5. Enable the JWT auth method
   ```
   % vault auth enable jwt
   Success! Enabled jwt auth method at: jwt/
   ```
6. Enable the user/password auth method
   ```shell
   % vault auth enable userpass
   Success! Enabled userpass auth method at: userpass/
   ```
7. Enable key/value secrets engine at `secrets/`
   ```shell
   % vault secrets enable -version=2 -path=secrets kv
   Success! Enabled the kv secrets engine at: secrets/
   ```
8. Create a policy for our initial user.
   ```shell
   % vault policy write secrets_admin - <<EOF
   path "secrets/*"
   {
   capabilities = ["create", "read", "update", "delete", "list"]
   }
   EOF
   Success! Uploaded policy: secrets_admin
   ```
9. Create your own user so you are not logging in as root constantly.
   ```shell
   % vault write auth/userpass/users/jsandlin password="fubar" policies="secrets_admin,default"
   Success! Data written to: auth/userpass/users/jsandlin
   ```
10. Configure jwt to trust GitLab.
   ```shell
    % vault write auth/jwt/config jwks_url="https://gitlab.com/-/jwks" bound_issuer="gitlab.com"
    Success! Data written to: auth/jwt/config
    ```
--------
#### Example Credential Setup & JWT Setup.
[REF](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/)
1. Login via `vault login -method=token` command above.
2. Create Read policy for secrets/demo
   ```shell
   % vault policy write demo_ro - <<EOF
   path "secrets/data/demo/*" {
      capabilities = ["read", "list"]
   }   
   EOF
   ```
4. Create a role linking JWT & the policy. Also included here are `bound_claims`, required to auth. 
   - Make certain you use the correct project id.
   ```
   % vault write auth/jwt/role/jwt_demo_ro - <<EOF
   {
     "role_type": "jwt",
     "policies": ["demo_ro"],
     "token_explicit_max_ttl": 60,
     "user_claim": "user_email",
     "bound_claims": {
       "project_id": "30561409"
     }
   }
   EOF
   ```
6. Create demo user (pick a better password than foo :))
   ```shell
   % vault write auth/userpass/users/demo password="foo" policies="demo_ro"
   Success! Data written to: auth/userpass/users/demo
   ```
7. Create a user so you don't always use root.
   ```shell
   % vault write auth/userpass/users/jsandlin policies="default, secrets_admin"
   ```
8. Auth as this user.
   ```shell
    % vault login -method=userpass username=jsandlin
    Password (will be hidden): 
    Success! You are now authenticated. The token information displayed below
    is already stored in the token helper. You do NOT need to run "vault login"
    again. Future Vault requests will automatically use this token.
    
    Key                    Value
    ---                    -----
    token                  xxx
    token_accessor         xxx
    token_duration         768h
    token_renewable        true
    token_policies         ["default" "secrets_admin"]
    identity_policies      []
    policies               ["default" "secrets_admin"]
    token_meta_username    jsandlin
    ```
9. Now let's write some example variables.
    ```shell
    % vault kv put secrets/demo/postgres/username value=pguser
    Key              Value
    ---              -----
    created_time     2021-11-10T17:04:42.694835339Z
    deletion_time    n/a
    destroyed        false
    version          1
    ```
    ```shell
    % vault kv put secrets/demo/postgres/password value=another_bad_password
    Key              Value
    ---              -----
    created_time     2021-11-10T17:05:54.491245795Z
    deletion_time    n/a
    destroyed        false
    version          1
    ```
10. [And our example project using this vault](https://gitlab.com/sandlin/examples/hashicorp_vault)

## REFS:
- [.gitlab-ci.yml secrets](https://docs.gitlab.com/ee/ci/yaml/index.html#secrets)
- [Edmond Chan's Vault examples](https://gitlab.com/edmond-demo/sandbox/hashicorp/vault-secrets-in-ci-jobs-gitlab-13.4/-/tree/master)
- [How to generate short-lived GCP Service Account Keys or OAuth2 tokens with Vault](https://amirsoleimani.medium.com/how-to-generate-short-lived-gcp-service-account-keys-or-oauth2-tokens-with-vault-ee906267d537)
- [Securely using secrets in a pipeline](https://amirsoleimani.medium.com/securely-using-secrets-in-a-pipeline-hashicorp-vault-jwt-auth-fa0a7eeb7e29)
- [Vault - Your First Secret](https://learn.hashicorp.com/tutorials/vault/getting-started-first-secret)
- [GitLab - Authenticating and reading secrets with HashiCorp Vault](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/)
- [letsencrypt-terraform](https://github.com/opencredo/letsencrypt-terraform/)
- [Automated Certificate Management Environment (ACME)](https://registry.terraform.io/providers/vancluever/acme/latest/docs)
- [Certbot](https://certbot.eff.org/docs/using.html#where-are-my-certificates)
- [Terraform & Lets Encrypt on GCP](https://blog.bryantluk.com/post/2018/06/02/terraform-and-lets-encrypt-on-google-cloud-platform/)
- [SSL Cert on Container Optimized OS](https://stackoverflow.com/questions/62493334/ssl-certificate-on-container-optimized-os-docker)
- [Terraform Google Vault Module](https://registry.terraform.io/modules/terraform-google-modules/vault/google/latest)
- [Terraform Google Vault Module - SRC](https://github.com/terraform-google-modules/terraform-google-vault)

<!-- Let's define some variables for ease of use. -->
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[issue]: https://gitlab.com/sandlin/terraform/hashicorp_vault/issues
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[pipeline-shield]: https://gitlab.com/sandlin/terraform/hashicorp_vault/badges/groot/pipeline.svg
