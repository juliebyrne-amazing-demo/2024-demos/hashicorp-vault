
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"

dns_zone_name   = "in-betweener"
fqdn    = "vault.in-betweener.com."

service_account_json = "/Users/jsandlin/.gcp/jsandlin-admin.json"
output_file = "../outputs/letsencrypt_dns_sa_creds.json.base64"

vault_allowed_cidrs = ["97.115.84.157", ]